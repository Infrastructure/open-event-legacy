apiVersion: v1
kind: Template
metadata:
  name: openevent-legacy-web
  annotations:
    openshift.io/display-name: "Open Event Platform (Legacy)"
    description:  >-
      Openshift template for Open Event platform (Legacy). For more information about using this template, see https://gitlab.gnome.org/Infrastructure/open-event-legacy/openshift/README.md
    openshift.io/long-description: >-
      This template creates the necessary resources to configure and deploy Open Event platform.
    tags: "events,organizer"
    iconClass: "fa fa-calendar"
    openshift.io/provider-display-name: "GNOME"
    openshift.io/documentation-url: "https://gitlab.gnome.org/Infrastructure/open-event-legacy/issues"
    openshift.io/support-url: "https://gitlab.gnome.org/Infrastructure/open-event-legacy/issues"
message: "The following passwords have been created in your project:\n\n Super Admin Password: ${ADMIN_PASSWORD}\n Redis User Password: ${REDIS_PASSWORD}\n Database User Password: ${DB_PASSWORD}"
objects:
### Deployment Configuration ###
- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    labels:
      io.kompose.service: backend
    name: backend
  spec:
    replicas: 1
    selector:
      io.kompose.service: backend
    strategy:
      activeDeadlineSeconds: 21600
      recreateParams:
        timeoutSeconds: 600
      resources: {}
      type: Recreate
    template:
      metadata:
        creationTimestamp: null
        labels:
          io.kompose.service: backend
      spec:
        containers:
        - env:
          - name: APP_NAMESPACE
            valueFrom:
              fieldRef:
                apiVersion: v1
                fieldPath: metadata.namespace
          - name: SUPER_ADMIN_EMAIL
            value: ${ADMIN_EMAIL}
          - name: SUPER_ADMIN_PASSWORD
            value: "${ADMIN_PASSWORD}"
          - name: APP_CONFIG
            value: config.ProductionConfig
          - name: DATABASE_URL
            value: postgresql://${DB_USER}:${DB_PASSWORD}@postgres.$(APP_NAMESPACE).svc:5432/${DB_NAME}
          - name: ENABLE_ELASTICSEARCH
            value: "false"
          - name: PORT
            value: "5000"
          - name: REDIS_URL
            value: redis://:${REDIS_PASSWORD}@redis.$(APP_NAMESPACE).svc:6379/0
          - name: SERVER_NAME
            value: ${BACKEND_URL}
          - name: FORCE_SSL
            value: "yes"
          - name: DEPLOYMENT
            value: web
          - name: C_FORCE_ROOT
            value: "true"
          - name: PYTHONUNBUFFERED
            value: "true"
          image: ${OPENEVENT_SERVER_IMAGE}
          imagePullPolicy: IfNotPresent
          name: ${NAME}-server
          ports:
          - containerPort: 5000
            protocol: TCP
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
          - mountPath: /open_event/static/uploads/
            name: static
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
        volumes:
        - name: static
          persistentVolumeClaim:
            claimName: static
    test: false
    triggers:
    - type: ConfigChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${NAME}-server
        from:
          kind: ImageStreamTag
          name: backend:development
      type: ImageChange
- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    generation: 4
    labels:
      io.kompose.service: celery
    name: celery
  spec:
    replicas: 1
    selector:
      io.kompose.service: celery
    strategy:
      activeDeadlineSeconds: 21600
      resources: {}
      rollingParams:
        intervalSeconds: 1
        maxSurge: 25%
        maxUnavailable: 25%
        timeoutSeconds: 600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        creationTimestamp: null
        labels:
          io.kompose.service: celery
      spec:
        containers:
        - env:
          - name: APP_NAMESPACE
            valueFrom:
              fieldRef:
                apiVersion: v1
                fieldPath: metadata.namespace
          - name: C_FORCE_ROOT
            value: "true"
          - name: DATABASE_URL
            value: postgresql://${DB_USER}:${DB_PASSWORD}@postgres.$(APP_NAMESPACE).svc:5432/${DB_NAME}
          - name: DEPLOYMENT
            value: celery
          - name: ENABLE_ELASTICSEARCH
            value: "false"
          - name: REDIS_URL
            value: redis://:${REDIS_PASSWORD}@redis.$(APP_NAMESPACE).svc:6379/0
          - name: SERVER_NAME
            value: ${BACKEND_URL}
          image: ${OPENEVENT_SERVER_IMAGE}
          imagePullPolicy: IfNotPresent
          name: ${NAME}-celery
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
            - mountPath: /open_event/static/uploads/
              name: static
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
        volumes:
          - name: static
            persistentVolumeClaim:
              claimName: static
    test: false
    triggers:
    - type: ConfigChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${NAME}-celery
        from:
          kind: ImageStreamTag
          name: backend:development
      type: ImageChange
- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    generation: 2
    labels:
      io.kompose.service: postgres
    name: postgres
  spec:
    replicas: 1
    selector:
      io.kompose.service: postgres
    strategy:
      activeDeadlineSeconds: 21600
      recreateParams:
        timeoutSeconds: 600
      resources: {}
      type: Recreate
    template:
      metadata:
        creationTimestamp: null
        labels:
          io.kompose.service: postgres
      spec:
        containers:
        - env:
          - name: POSTGRESQL_DATABASE
            value: ${DB_NAME}
          - name: POSTGRESQL_PASSWORD
            value: ${DB_PASSWORD}
          - name: POSTGRESQL_USER
            value: ${DB_USER}
          image: ${POSTGRESQL_IMAGE}
          imagePullPolicy: IfNotPresent
          name: ${NAME}-postgres
          ports:
          - containerPort: 5432
            protocol: TCP
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
          - mountPath: /var/lib/pgsql/data
            name: psql
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
        volumes:
        - name: psql
          persistentVolumeClaim:
            claimName: psql
    test: false
    triggers:
    - type: ConfigChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${NAME}-postgres
        from:
          kind: ImageStreamTag
          name: postgres:9.5
      type: ImageChange
- apiVersion: apps.openshift.io/v1
  kind: DeploymentConfig
  metadata:
    labels:
      io.kompose.service: redis
    name: redis
  spec:
    replicas: 1
    selector:
      io.kompose.service: redis
    strategy:
      activeDeadlineSeconds: 21600
      recreateParams:
        timeoutSeconds: 600
      resources: {}
      type: Recreate
    template:
      metadata:
        creationTimestamp: null
        labels:
          io.kompose.service: redis
      spec:
        containers:
        - env:
            - name: REDIS_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: database-password
                  name: redis
          image: redis:3.2
          command:
            - "container-entrypoint"
          args:
            - "run-redis"
          imagePullPolicy: IfNotPresent
          name: ${NAME}-redis
          ports:
          - containerPort: 6379
            protocol: TCP
          livenessProbe:
            failureThreshold: 3
            initialDelaySeconds: 30
            periodSeconds: 10
            successThreshold: 1
            tcpSocket:
              port: 6379
            timeoutSeconds: 1
          readinessProbe:
            exec:
              command:
                - /bin/sh
                - '-i'
                - '-c'
                - >-
                  test "$(redis-cli -h 127.0.0.1 -a $REDIS_PASSWORD ping)" ==
                  "PONG"
            failureThreshold: 3
            initialDelaySeconds: 5
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
          resources: {}
          terminationMessagePath: /dev/termination-log
          terminationMessagePolicy: File
          volumeMounts:
          - mountPath: /var/lib/redis/data
            name: redis
        dnsPolicy: ClusterFirst
        restartPolicy: Always
        schedulerName: default-scheduler
        securityContext: {}
        terminationGracePeriodSeconds: 30
        volumes:
        - name: redis
          persistentVolumeClaim:
            claimName: redis
    test: false
    triggers:
    - type: ConfigChange
    - imageChangeParams:
        automatic: true
        containerNames:
        - ${NAME}-redis
        from:
          kind: ImageStreamTag
          name: "redis:3.2"
          namespace: openshift
      type: ImageChange
### Build Config ###
- apiVersion: build.openshift.io/v1
  kind: "BuildConfig"
  metadata:
    name: "backend"
  spec:
    runPolicy: "Serial"
    source:
      git:
        uri: https://gitlab.gnome.org/Infrastructure/open-event-legacy.git
        ref: deploy
    strategy:
      dockerStrategy:
        dockerfilePath: Dockerfile
    output:
      to:
        kind: "ImageStreamTag"
        name: backend:development

### Image Streams ###
- apiVersion: image.openshift.io/v1
  kind: ImageStream
  metadata:
    generation: 2
    labels:
      io.kompose.service: backend
    name: backend
  spec:
    lookupPolicy:
      local: false
    tags:
    - annotations: null
      from:
        kind: DockerImage
        name: backend:development
      generation: 2
      importPolicy: {}
      name: development
      referencePolicy:
        type: Source
- apiVersion: image.openshift.io/v1
  kind: ImageStream
  metadata:
    labels:
      io.kompose.service: postgres
    name: postgres
  spec:
    lookupPolicy:
      local: false
    tags:
    - annotations: null
      from:
        kind: DockerImage
        name: centos/postgresql-95-centos7:9.5
      importPolicy: {}
      name: "9.5"
      referencePolicy:
        type: Source
### Services ###
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      io.kompose.service: backend
    name: backend
  spec:
    ports:
    - name: "8080"
      port: 8080
      targetPort: 8080
    selector:
      io.kompose.service: backend
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      io.kompose.service: postgres
    name: postgres
  spec:
    ports:
    - name: "5432"
      port: 5432
      protocol: TCP
      targetPort: 5432
    selector:
      io.kompose.service: postgres
- apiVersion: v1
  kind: Service
  metadata:
    labels:
      io.kompose.service: redis
    name: redis
  spec:
    ports:
    - name: "6379"
      port: 6379
      protocol: TCP
      targetPort: 6379
    selector:
      io.kompose.service: redis
### Routes ###
- apiVersion: route.openshift.io/v1
  kind: Route
  metadata:
    labels:
      io.kompose.service: backend
    name: backend
  spec:
    host: ${BACKEND_URL}
    port:
      targetPort: "8080"
    to:
      kind: Service
      name: backend
    tls:
      insecureEdgeTerminationPolicy: "Redirect"
      termination: "edge"
### Secrets ###
- apiVersion: v1
  kind: Secret
  metadata:
    name: redis
  stringData:
    database-password: ${REDIS_PASSWORD}
### Persistent Storage ###
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    creationTimestamp: null
    labels:
      io.kompose.service: static
    name: static
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: 10240mB
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    creationTimestamp: null
    labels:
      io.kompose.service: redis
    name: redis
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: 5120mB
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    creationTimestamp: null
    labels:
      io.kompose.service: psql
    name: psql
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: 5120mB
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    creationTimestamp: null
    labels:
      io.kompose.service: ${DATABASE_BACKUP_VOLUME_CLAIM}
    name: ${DATABASE_BACKUP_VOLUME_CLAIM}
  spec:
    accessModes:
    - ReadWriteOnce
    resources:
      requests:
        storage: 5120mB
### Cron Jobs ###
- apiVersion: batch/v1beta1
  kind: CronJob
  metadata:
    name: postgres-backup
  spec:
    schedule: "${DATABASE_BACKUP_SCHEDULE}"
    concurrencyPolicy: Forbid
    jobTemplate:
      spec:
        template:
          spec:
            volumes:
              - name: database-backup
                persistentVolumeClaim:
                  claimName: ${DATABASE_BACKUP_VOLUME_CLAIM}
            containers:
              - name: database-backup
                image: 'registry.access.redhat.com/rhscl/postgresql-96-rhel7:1'
                command:
                  - 'bash'
                  - '-eo'
                  - 'pipefail'
                  - '-c'
                  - > 
                    trap "echo Backup failed; exit 0" ERR; 
                    FILENAME=backup-${DATABASE_NAME}-`date +%Y-%m-%d_%H%M%S`.sql.gz;
                    time (find /database-backup -type f -name "backup-${DATABASE_NAME}-*"  -exec ls -1tr "{}" + | head -n -${DATABASE_BACKUP_KEEP} | xargs rm -fr; 
                    PGPASSWORD="$DATABASE_PASSWORD" pg_dump --username=$DATABASE_USER --host=$DATABASE_HOST --port=5432 --column-inserts --clean --create ${DATABASE_NAME} | gzip > /database-backup/$FILENAME); 
                    echo "";
                    echo "Backup successful"; du -h /database-backup/$FILENAME; 
                    echo "to restore the backup to the serviced host use: $ psql --username=$DATABASE_USER --password --host=$DATABASE_HOST --port=5432 postgres < /database-backup/<backupfile> (unpacked)"
                env:
                  - name: APP_NAMESPACE
                    valueFrom:
                      fieldRef:
                        apiVersion: v1
                        fieldPath: metadata.namespace
                  - name: DATABASE_BACKUP_KEEP
                    value: ${DATABASE_BACKUP_KEEP}
                  - name: DATABASE_USER
                    value: ${DB_USER}
                  - name: DATABASE_PASSWORD
                    value: ${DB_PASSWORD}
                  - name: DATABASE_HOST
                    value: postgres.$(APP_NAMESPACE).svc
                  - name: DATABASE_NAME
                    value: ${DB_NAME}
                  - name: TZ
                    value: Europe/Zurich
                volumeMounts:
                  - name: database-backup
                    mountPath: /database-backup
            restartPolicy: Never
parameters:
- name: NAME
  displayName: Name
  description: The name assigned to all of the services defined in this template.
  required: true
  value: "openevent-legacy-web"
- name: BACKEND_URL
  displayName: API Server Domain Name
  description: URL for the API server (Do not include any protocol).
  required: true
  value: "events.gnome.org"
- name: ADMIN_EMAIL
  displayName: Super Admin Email Address
  description: Email address for Super Admin account.
  required: true
  value: "events@gnome.org"
- name: ADMIN_PASSWORD
  displayName: Super Admin Password
  description: Super admin password for Open Event platform.
  from: "[a-zA-Z0-9]{20}"
  generate: expression
  required: true
- name: DB_NAME
  displayName: Database Name
  description: Database Name for PostgreSQL.
  value: "open_event"
  required: true
- name: DB_USER
  displayName: Database User
  description: Database User for PostgreSQL.
  value: "open_event_user"
  required: true
- name: DB_PASSWORD
  displayName: Database User Password
  description: Database Password for PostgreSQL.
  from: "[a-zA-Z0-9]{20}"
  generate: expression
  required: true
- name: POSTGRESQL_IMAGE
  displayName: PostgreSQL Image
  description: PostgreSQL image to use.
  value: "centos/postgresql-95-centos7"
  required: true
- name: REDIS_PASSWORD
  displayName: Redis User Password
  description: Password for the Redis connection user.
  from: "[a-zA-Z0-9]{20}"
  generate: expression
  required: true
- name: OPENEVENT_SERVER_IMAGE
  displayName: Open Event Backend Image
  description: Image to use for Open Event backend servers.
  value: "backend:development"
  required: true
- name: DATABASE_BACKUP_KEEP
  displayName: 
  description: Number of backups to keep.
  value: "7"
- name: DATABASE_BACKUP_SCHEDULE
  displayName: Database Backup Schedule
  description: Cron-like schedule expression.
  value: "55 * * * *"
- name: DATABASE_BACKUP_VOLUME_CLAIM
  description: Name of the volume claim to be used as storage.
  value: "psql-backups"
  required: true
